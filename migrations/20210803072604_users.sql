-- Add migration script here
CREATE TABLE users(
    uuid TEXT PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    phc TEXT NOT NULL,
    unique (name)
);