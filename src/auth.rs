use std::sync::Arc;

use axum::{extract, response, response::IntoResponse};
use chrono::{DateTime, Duration, Utc};
use http::Response;
use hyper::Body;
use jsonwebtoken::{encode, Header};
use serde::{Deserialize, Serialize};
use sqlx::SqlitePool;
use uuid::Uuid;

use crate::{
	error::{FileUploadError, FileUploadErrorCode},
	schema,
	settings::Settings,
};

#[derive(Debug, Deserialize)]
pub struct CredentialParams {
	pub username: String,
	pub password: String,
}

#[derive(Debug, Deserialize)]
pub struct ChangeCredentialParams {
	pub username: String,
	pub password: String,
	pub new_username: Option<String>,
	pub new_password: Option<String>,
}

#[derive(Debug, Serialize)]
pub struct RegistrationResponse {
	pub uuid: Uuid,
	pub username: String,
}

#[serde_with::serde_as]
#[derive(Debug, Serialize, Deserialize)]
pub struct TokenClaims {
	pub uuid: Uuid,
	#[serde_as(as = "serde_with::TimestampSeconds")]
	pub exp: DateTime<Utc>,
}

pub async fn register(
	settings: extract::Extension<Arc<Settings>>,
	pool: extract::Extension<SqlitePool>,
	payload: extract::Json<CredentialParams>,
) -> Result<Response<Body>, FileUploadError> {
	if settings.auth.registration {
		if schema::User::from_user(&pool, payload.username.as_str()).await?.is_some() {
			Err(FileUploadError::new(
				FileUploadErrorCode::USER_ALREADY_EXISTS,
				"User already exists",
			))
		} else {
			let user =
				schema::User::new(&pool, payload.username.as_str(), payload.password.as_str())
					.await?;
			Ok(response::Json(RegistrationResponse { uuid: user.uuid, username: user.name })
				.into_response())
		}
	} else {
		Err(FileUploadError::new(
			FileUploadErrorCode::REGISTRATION_DISABLED,
			"Registration Disabled",
		))
	}
}

pub async fn tokenpls(
	settings: extract::Extension<Arc<Settings>>,
	pool: extract::Extension<SqlitePool>,
	payload: extract::Query<CredentialParams>,
) -> Result<String, FileUploadError> {
	let user = match schema::User::from_user(&pool, payload.username.as_str()).await? {
		Some(user) => user,
		None => return Err(FileUploadError::new(FileUploadErrorCode::AUTH_FAILED, "No User")),
	};

	if user.validate(payload.password.as_str()).await {
		encode(
			&Header::new(settings.auth.jwt.algorithm),
			&TokenClaims {
				uuid: user.uuid,
				exp: Utc::now()
					+ Duration::seconds(settings.auth.jwt.expires_in.unwrap_or(69420) as i64),
			},
			&settings.auth.jwt.private_key,
		)
		.map_err(|e| {
			log::error!("JWT Error: {}", e);
			FileUploadError::new(FileUploadErrorCode::AUTH_FAILED, "JWT Error")
		})
	} else {
		Err(FileUploadError::new(FileUploadErrorCode::AUTH_FAILED, "Bad Password"))
	}
}

pub async fn change_details(
	pool: extract::Extension<SqlitePool>,
	payload: extract::Json<ChangeCredentialParams>, /* Only allow username + password combo to
	                                                 * change credentials to stop compromised
	                                                 * tokens taking over accounts */
) -> Result<(), FileUploadError> {
	let user = match schema::User::from_user(&pool, payload.username.as_str()).await? {
		Some(user) => user,
		None => return Err(FileUploadError::new(FileUploadErrorCode::AUTH_FAILED, "No User")),
	};

	if user.validate(payload.password.as_str()).await {
		#[allow(clippy::or_fun_call)]
		user.update(
			&pool,
			payload.new_username.as_deref().unwrap_or(user.name.as_str()),
			payload.new_password.as_deref().unwrap_or(payload.password.as_str()),
		)
		.await?;
		Ok(())
	} else {
		Err(FileUploadError::new(FileUploadErrorCode::AUTH_FAILED, "Bad Password"))
	}
}
