use axum::extract::multipart::MultipartError;
use famedly_axum_error::famedly_error;

famedly_error!(FileUploadError {
	INVALID_PATH: BAD_REQUEST,
	UPLOAD_ERROR: INTERNAL_SERVER_ERROR,
	DATABASE_ERROR: INTERNAL_SERVER_ERROR,
	FILE_NOT_FOUND: NOT_FOUND,
	FILE_READ_ERROR: INTERNAL_SERVER_ERROR,
	HTTP_ERROR: INTERNAL_SERVER_ERROR,
	REGISTRATION_DISABLED: UNAUTHORIZED,
	USER_ALREADY_EXISTS: UNAUTHORIZED,
	AUTH_FAILED: UNAUTHORIZED
});

impl From<MultipartError> for FileUploadError {
	fn from(err: MultipartError) -> Self {
		Self::new(FileUploadErrorCode::UPLOAD_ERROR, format!("{:?}", err))
	}
}

impl From<sqlx::Error> for FileUploadError {
	fn from(error: sqlx::Error) -> Self {
		log::error!("DB Error: {:?}", error);
		Self::new(FileUploadErrorCode::DATABASE_ERROR, "Database Error")
	}
}
