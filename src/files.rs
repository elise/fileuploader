use std::sync::Arc;

use axum::{
	extract::{self, UrlParams},
	response::{IntoResponse, Json},
};
use http::Response;
use hyper::Body;
use regex::Regex;
use sqlx::SqlitePool;
use uuid::Uuid;

use crate::{
	error::{FileUploadError, FileUploadErrorCode},
	schema,
	settings::Settings,
};

pub async fn upload(
	settings: extract::Extension<Arc<Settings>>,
	pool: extract::Extension<SqlitePool>,
	mut multipart: extract::Multipart,
) -> Result<Response<Body>, FileUploadError> {
	lazy_static::lazy_static! {
		static ref FILE_REGEX: Regex = Regex::new(r#"^[^<>:;,?"*|/\\]+$"#).unwrap();
	}

	let mut files = Vec::new();

	while let Some(field) = multipart.next_field().await? {
		if let Some(file_path_str) = field.file_name() {
			let mut uuid = Uuid::new_v4();

			while schema::FileMetadata::from_uuid(&pool, &uuid).await?.is_some() {
				log::error!("Wew! Rolled a UUID collision, retrying");
				uuid = Uuid::new_v4();
			}

			if !FILE_REGEX.is_match(file_path_str) {
				return Err(FileUploadError::new(
					FileUploadErrorCode::INVALID_PATH,
					"Invalid File Namex",
				));
			}

			let metadata = schema::FileMetadata { uuid, name: file_path_str.to_string() };

			let path = metadata.path(&settings.upload.directory);

			tokio::fs::write(path, field.bytes().await?).await.map_err(|error| {
				log::error!("Write error: {}", error);
				FileUploadError::new(FileUploadErrorCode::UPLOAD_ERROR, "Write Error")
			})?;

			metadata.insert(&pool).await?;

			files.push(metadata.uuid.to_string());
		}
	}

	if files.is_empty() {
		Err(FileUploadError::new(FileUploadErrorCode::HTTP_ERROR, "File form not found"))
	} else {
		Ok(Json(files).into_response())
	}
}

pub async fn download(
	settings: extract::Extension<Arc<Settings>>,
	pool: extract::Extension<SqlitePool>,
	UrlParams((uuid,)): UrlParams<(Uuid,)>,
) -> Result<Response<hyper::Body>, FileUploadError> {
	let metadata = match schema::FileMetadata::from_uuid(&pool, &uuid).await? {
		Some(metadata) => metadata,
		None => return Err(FileUploadError::new(FileUploadErrorCode::FILE_NOT_FOUND, "Not Found")),
	};

	let path = metadata.path(&settings.upload.directory);

	if !path.exists() || !path.is_file() {
		return Err(FileUploadError::new(FileUploadErrorCode::FILE_NOT_FOUND, "Not Found"));
	}

	let file = tokio::fs::File::open(&path).await.map_err(|e| {
		log::error!("Failed to open file {:?} ({})", path, e);
		FileUploadError::new(FileUploadErrorCode::FILE_READ_ERROR, "Internal Read Error")
	})?;

	let stream = tokio_util::io::ReaderStream::new(file);

	let mime = new_mime_guess::from_path(&metadata.name).first_or_octet_stream();

	Response::builder()
		.header("Content-Type", mime.to_string())
		.header(
			"Content-Disposition",
			format!("attachment; filename*=UTF-8''{}", urlencoding::encode(metadata.name.as_str())),
		)
		.body(hyper::Body::wrap_stream(stream))
		.map_err(|e| {
			log::error!("HTTP Error: {}", e);
			FileUploadError::new(FileUploadErrorCode::HTTP_ERROR, "Internal HTTP Error")
		})
}
