mod auth;
mod error;
mod files;
mod middleware;
mod schema;
mod settings;
mod templates;

use std::sync::Arc;

use axum::{
	prelude::{get, post, RoutingDsl},
	route, AddExtensionLayer,
};
use sqlx::{migrate, SqlitePool};

use crate::middleware::TokenAuth;

/*fn sanitize_upload_path(settings: &Settings, path: &Path) -> Result<PathBuf, FileUploadError> {
	let canonicalized = std::fs::canonicalize(path).map_err(|e| {
		log::error!("Invalid path upload attempted: {:?} ({})", path, e);
		FileUploadError::new(FileUploadErrorCode::INVALID_PATH, "Invalid Path")
	})?;

	if !canonicalized.is_file() {
		Err(FileUploadError::new(FileUploadErrorCode::INVALID_PATH, "Path not a file"))
	} else if canonicalized.parent() == settings.upload.directory.parent() {
		Err(FileUploadError::new(FileUploadErrorCode::INVALID_PATH, "*bonks* no path traversal"))
	} else {
		Ok(canonicalized)
	}
}*/

#[tokio::main]
async fn main() {
	env_logger::init();

	log::info!("Starting");

	let settings = match settings::Settings::new() {
		Ok(settings) => Arc::new(settings),
		Err(why) => {
			log::error!("Config is invalid: {}", why);
			std::process::exit(1);
		}
	};

	let pool = match SqlitePool::connect(settings.database_url.as_str()).await {
		Ok(pool) => pool,
		Err(why) => {
			log::error!("DB Connection failure: {}", why);
			return;
		}
	};

	migrate!().run(&pool).await.expect("Migration Error");

	let app = route("/download/:uuid", TokenAuth::new(get(files::download), settings.clone()))
		.route("/upload", post(files::upload))
		.route("/auth", get(auth::tokenpls).post(auth::register).patch(auth::change_details))
		.route("/", get(templates::template))
		.layer(AddExtensionLayer::new(pool.clone()))
		.layer(AddExtensionLayer::new(settings.clone()));

	hyper::Server::bind(&settings.bind).serve(app.into_make_service()).await.unwrap()
}
