use std::{pin::Pin, sync::Arc, task::Poll};

use axum::body::{box_body, BoxStdError};
use futures::Future;
use http::Request;
use http_body::combinators::BoxBody;
use hyper::body::Bytes;
use jsonwebtoken::{decode, Validation};
use tower::Service;

use crate::{
	auth::TokenClaims,
	error::{FileUploadError, FileUploadErrorCode},
	settings::Settings,
};
#[derive(Debug, Clone)]
pub struct TokenAuth<Svc> {
	inner: Svc,
	settings: Arc<Settings>,
	validation: Validation,
}

impl<Svc> TokenAuth<Svc> {
	pub fn new(inner: Svc, settings: Arc<Settings>) -> Self {
		let alg = settings.auth.jwt.algorithm;
		TokenAuth { inner, settings, validation: Validation::new(alg) }
	}
}

#[derive(Debug)]
pub enum AuthErrorKind {
	MissingHeader,
	InvalidHeader,
	InvalidToken,
	ExpiredToken,
	JsonWebToken(jsonwebtoken::errors::Error),
}

impl<Svc, ReqBody> Service<Request<ReqBody>> for TokenAuth<Svc>
where
	Svc: Service<Request<ReqBody>, Response = http::Response<BoxBody<Bytes, BoxStdError>>>,
{
	type Response = http::Response<BoxBody<Bytes, BoxStdError>>;

	type Error = Svc::Error;

	type Future = TokenAuthFuture<Svc::Future>;

	fn poll_ready(
		&mut self,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), Self::Error>> {
		self.inner.poll_ready(cx).map_err(Into::into)
	}

	fn call(&mut self, req: Request<ReqBody>) -> Self::Future {
		let auth_error = if let Some(auth_header) = req.headers().get("Authorization") {
			if let Ok(val) = auth_header.to_str() {
				if val.starts_with("Bearer ") {
					let token = &val[7..val.len()];
					match decode::<TokenClaims>(
						token,
						&self.settings.auth.jwt.public_key,
						&self.validation,
					) {
						Ok(_) => None,
						Err(why) => match why.kind() {
							jsonwebtoken::errors::ErrorKind::InvalidToken
							| jsonwebtoken::errors::ErrorKind::InvalidSignature
							| jsonwebtoken::errors::ErrorKind::InvalidAlgorithmName
							| jsonwebtoken::errors::ErrorKind::InvalidIssuer
							| jsonwebtoken::errors::ErrorKind::InvalidAudience
							| jsonwebtoken::errors::ErrorKind::InvalidSubject
							| jsonwebtoken::errors::ErrorKind::InvalidAlgorithm => Some(AuthErrorKind::InvalidToken),
							jsonwebtoken::errors::ErrorKind::ImmatureSignature
							| jsonwebtoken::errors::ErrorKind::ExpiredSignature => Some(AuthErrorKind::ExpiredToken),
							_ => Some(AuthErrorKind::JsonWebToken(why)),
						},
					}
				} else {
					Some(AuthErrorKind::InvalidHeader)
				}
			} else {
				Some(AuthErrorKind::InvalidHeader)
			}
		} else {
			Some(AuthErrorKind::MissingHeader)
		};

		TokenAuthFuture::new(self.inner.call(req), auth_error)
	}
}

#[pin_project::pin_project]
pub struct TokenAuthFuture<F> {
	#[pin]
	response_future: F,
	#[pin]
	error: Option<AuthErrorKind>,
}

impl<F> TokenAuthFuture<F> {
	pub fn new(response_future: F, error: Option<AuthErrorKind>) -> Self {
		Self { response_future, error }
	}
}
use axum::response::IntoResponse;
impl<F, Error> Future for TokenAuthFuture<F>
where
	F: Future<Output = Result<http::Response<BoxBody<Bytes, BoxStdError>>, Error>>,
{
	type Output = Result<http::Response<BoxBody<Bytes, BoxStdError>>, Error>;

	fn poll(
		self: std::pin::Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Self::Output> {
		match &self.error {
			Some(err) => Poll::Ready(Ok(FileUploadError::new(
				FileUploadErrorCode::AUTH_FAILED,
				format!("Auth failed: {:?}", err),
			)
			.into_response()
			.map(box_body))),
			None => {
				let this = self.project();
				let response_future: Pin<&mut F> = this.response_future;
				response_future.poll(cx).map(|res| res.map_err(Into::into))
			}
		}
	}
}
