use std::path::{Path, PathBuf};

use argon2::{password_hash::SaltString, Argon2, PasswordHash, PasswordHasher, PasswordVerifier};
use sqlx::{query, query_as, FromRow, SqlitePool};
use uuid::Uuid;

#[derive(Debug, FromRow)]
pub struct FileMetadata {
	pub uuid: Uuid,
	pub name: String,
}

impl FileMetadata {
	pub async fn from_uuid(pool: &SqlitePool, uuid: &Uuid) -> sqlx::Result<Option<Self>> {
		query_as!(Self, r#"SELECT uuid as "uuid: Uuid", name FROM files WHERE uuid = $1"#, uuid)
			.fetch_optional(pool)
			.await
	}

	pub async fn insert(&self, pool: &SqlitePool) -> sqlx::Result<sqlx::sqlite::SqliteQueryResult> {
		query!(r#"INSERT INTO files (uuid, name) VALUES ($1, $2)"#, self.uuid, self.name)
			.execute(pool)
			.await
	}

	pub fn path(&self, dir: &Path) -> PathBuf {
		let mut path = dir.to_path_buf();
		path.push(self.uuid.to_string());
		path
	}
}

#[derive(Debug, FromRow)]
pub struct User {
	pub uuid: Uuid,
	pub name: String,
	pub phc: String,
}

impl User {
	pub async fn new(pool: &SqlitePool, name: &str, password: &str) -> sqlx::Result<Self> {
		let mut uuid = Uuid::new_v4();

		while Self::from_uuid(pool, &uuid).await?.is_some() {
			log::error!("Wew! Rolled a 	UUID collision, retrying");
			uuid = Uuid::new_v4();
		}

		let salt = SaltString::generate(&mut rand_core::OsRng);

		let phc = Argon2::default()
			.hash_password_simple(password.as_bytes(), salt.as_ref())
			.unwrap()
			.to_string();

		let s = Self { uuid, name: name.to_string(), phc };

		query!(r#"INSERT INTO users (uuid, name, phc) VALUES ($1, $2, $3)"#, s.uuid, s.name, s.phc)
			.execute(pool)
			.await?;

		Ok(s)
	}

	pub async fn from_uuid(pool: &SqlitePool, uuid: &Uuid) -> sqlx::Result<Option<Self>> {
		query_as!(
			Self,
			r#"SELECT uuid as "uuid: Uuid", name, phc FROM users WHERE uuid = $1"#,
			uuid
		)
		.fetch_optional(pool)
		.await
	}

	pub async fn from_user(pool: &SqlitePool, name: &str) -> sqlx::Result<Option<Self>> {
		query_as!(
			Self,
			r#"SELECT uuid as "uuid: Uuid", name, phc FROM users WHERE name = $1"#,
			name
		)
		.fetch_optional(pool)
		.await
	}

	pub async fn validate(&self, password: &str) -> bool {
		let parsed_hash = match PasswordHash::new(&self.phc) {
			Ok(hash) => hash,
			Err(why) => {
				log::error!("Corrupted PHC ended up in DB ({})", why);
				return false;
			}
		};

		Argon2::default().verify_password(password.as_bytes(), &parsed_hash).is_ok()
	}

	pub async fn update(&self, pool: &SqlitePool, name: &str, password: &str) -> sqlx::Result<()> {
		let salt = SaltString::generate(&mut rand_core::OsRng);

		let phc = Argon2::default()
			.hash_password_simple(password.as_bytes(), salt.as_ref())
			.unwrap()
			.to_string();

		query!("UPDATE users SET name = $1, phc = $2 WHERE uuid = $3", name, phc, self.uuid)
			.execute(pool)
			.await?;

		Ok(())
	}
}
