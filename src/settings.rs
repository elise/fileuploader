use std::{net::SocketAddr, path::PathBuf};

use config::ConfigError;
use jsonwebtoken::{Algorithm, DecodingKey, EncodingKey};
use serde::{Deserialize, Deserializer, Serialize};

pub fn deserialize_canonicalized_directory_path_buf<'de, D>(
	deserializer: D,
) -> Result<PathBuf, D::Error>
where
	D: Deserializer<'de>,
{
	let path = PathBuf::deserialize(deserializer)?;

	std::fs::create_dir_all(&path).map_err(serde::de::Error::custom)?;

	if !path.is_dir() {
		return Err(serde::de::Error::custom("not a directory"));
	}

	let canonicalized = std::fs::canonicalize(path).map_err(serde::de::Error::custom)?;

	if canonicalized.parent().is_none() {
		return Err(serde::de::Error::custom("pls no use on root directory"));
	}

	Ok(canonicalized)
}

#[derive(Clone, Debug, Deserialize)]
pub struct UploadSettings {
	#[serde(deserialize_with = "deserialize_canonicalized_directory_path_buf")]
	pub directory: PathBuf,
}

/// Given the key and an algorithm it will return an [`EncodingKey`](https://docs.rs/jsonwebtoken/7.2.0/jsonwebtoken/struct.EncodingKey.html)
pub fn make_encoding_key(
	key: &[u8],
	algorithm: Algorithm,
) -> jsonwebtoken::errors::Result<EncodingKey> {
	match algorithm {
		Algorithm::HS256 | Algorithm::HS384 | Algorithm::HS512 => Ok(EncodingKey::from_secret(key)),
		Algorithm::ES256 | Algorithm::ES384 => EncodingKey::from_ec_pem(key),
		Algorithm::RS256
		| Algorithm::RS384
		| Algorithm::RS512
		| Algorithm::PS256
		| Algorithm::PS384
		| Algorithm::PS512 => EncodingKey::from_rsa_pem(key),
	}
}

/// Given the key and an algorithm it will return a [`DecodingKey`](https://docs.rs/jsonwebtoken/7.2.0/jsonwebtoken/struct.DecodingKey.html)
pub fn make_decoding_key(
	key: &[u8],
	algorithm: Algorithm,
) -> jsonwebtoken::errors::Result<DecodingKey<'_>> {
	match algorithm {
		Algorithm::HS256 | Algorithm::HS384 | Algorithm::HS512 => Ok(DecodingKey::from_secret(key)),
		Algorithm::ES256 | Algorithm::ES384 => {
			DecodingKey::from_ec_pem(key).map(DecodingKey::into_static)
		}
		Algorithm::RS256
		| Algorithm::RS384
		| Algorithm::RS512
		| Algorithm::PS256
		| Algorithm::PS384
		| Algorithm::PS512 => DecodingKey::from_rsa_pem(key),
	}
}
/// Parsed key representation of [JwtKeyConfig], implements Deserialize via
/// [JwtKeyConfig]
#[derive(Clone, Debug)]
pub struct JwtParsedKeyConfig {
	/// Algorithm for Signatures and occasionally encryption/decryption
	pub algorithm: Algorithm,
	/// Optional offset for token expiry in seconds
	pub expires_in: Option<u32>,
	/// Key used for signature validation or decryption of tokens
	pub public_key: DecodingKey<'static>,
	/// Key used for signing or encryption of tokens
	pub private_key: EncodingKey,
	/// Unparsed key used for signature validation or decryption of tokens
	pub unparsed_public_key: String,
	/// Unparsed key used for signing or encryption of tokens
	pub unparsed_private_key: String,
}

impl<'de> Deserialize<'de> for JwtParsedKeyConfig {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		JwtKeyConfig::deserialize(deserializer)?
			.parse_keys()
			.map_err(|e| serde::de::Error::custom(e.to_string()))
	}
}

/// Unparsed JWT config
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct JwtKeyConfig {
	/// Algorithm for Signatures and occasionally encryption/decryption
	pub algorithm: Algorithm,
	/// Optional offset for token expiry in seconds
	pub expires_in: Option<u32>,
	/// Unparsed key used for signature validation or decryption of tokens
	pub public_key: String,
	/// Unparsed key used for signing or encryption of tokens
	pub private_key: String,
}

impl JwtKeyConfig {
	/// Parses the keys for `jsonwebtoken` and returns a new
	/// `JwtParsedKeyConfig`
	pub fn parse_keys(&self) -> jsonwebtoken::errors::Result<JwtParsedKeyConfig> {
		Ok(JwtParsedKeyConfig {
			algorithm: self.algorithm,
			expires_in: self.expires_in,
			public_key: self.decoding_key()?,
			private_key: self.encoding_key()?,
			unparsed_public_key: self.public_key.clone(),
			unparsed_private_key: self.private_key.clone(),
		})
	}

	/// Parses the encoding key for `jsonwebtoken`
	pub fn encoding_key(&self) -> jsonwebtoken::errors::Result<EncodingKey> {
		make_encoding_key(self.private_key.as_bytes(), self.algorithm)
	}

	/// Parses the decoding key for `jsonwebtoken`
	pub fn decoding_key(&self) -> jsonwebtoken::errors::Result<DecodingKey<'static>> {
		make_decoding_key(self.public_key.as_bytes(), self.algorithm).map(DecodingKey::into_static)
	}
}

fn dtrue() -> bool {
	true
}

#[derive(Clone, Debug, Deserialize)]
pub struct AuthSettings {
	#[serde(default = "dtrue")]
	pub registration: bool,
	pub jwt: JwtParsedKeyConfig,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Settings {
	pub auth: AuthSettings,
	pub upload: UploadSettings,
	pub database_url: String,
	pub bind: SocketAddr,
}

impl Settings {
	pub fn new() -> Result<Self, ConfigError> {
		let mut settings = config::Config::default();
		settings
			.merge(config::File::with_name("uploader"))
			.unwrap()
			.merge(config::Environment::with_prefix("FILE_UPLOAD"))
			.unwrap();

		settings.try_into::<Self>()
	}
}
