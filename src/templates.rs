use axum::response::Html;
use html_to_string_macro::html;

pub async fn template() -> Html<String> {
	Html(html! {
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8"/>
				<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				<title>"Uploader"</title>
				<meta name="description" content="UwU"/>
				<meta name="viewport" content="width=device-width, initial-scale=1"/>
				<link rel="stylesheet" href=""/>
			</head>
			<body>
				<form method="POST" enctype="multipart/form-data" action="/upload">
					<input type="file" id="file" name="filename"/>
					<input type="submit"/>
				</form>
				<script src="" async defer></script>
			</body>
		</html>
	})
}
